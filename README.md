# Attribute Verification

### Introduction
This Powershell script will automatically verify users attributes in an AD OU, and email report any users that do not match

### How To Use
Script will use a file called Attributes.csv located in it's directory (unless otherwise specified)
Explaination of columns of that CSV. Starting with the first column, and moving to the right:

OrganizationalUnit: This is the OU that you want the script to look in to verify attributes
Attribute: This is the name AD attribute you wish to verify. Example: title
Attribute Value X: The third and further columns will be the acceptable values Example: "HR Manager"

### Example Email_Creds.json
[{
    "__comment"     :   "Special characters may need a backslash before them",
    "SmtpUsername"     :   "",
    "SmtpPassword"    :   "",
    "SmtpServer"     :    "",
    "SmtpPort" :      ""
}]

SmtpServer will be something like (if using O365): example-com.mail.protection.outlook.com
