#
# This script will verify user attributes in on-prem AD
#
# By: Landon (landon@almonde.org)
# Version: 2022-02-08


#############################
# Variables
#############################
# Export path for logging info (csv)
$CsvLogPath = ".\EmployeeIDLog.csv"
# Delimiter of log CSV, pipe is strongly recommended "|"
$CsvDelimiter = "|"
# Number of how many possible attributes the script will add to the CSV file and read from it
$CsvAttributesNumber = 15
# AD servers to pull from. Parent and/or child domains
$ADServers = @('subdomain.example.com','example.com')
# Path to csv list of attributes to verify
$CsvAttributesPath = ".\Attributes.csv"
# Usernames to ignore for purposes of script (Examples: Service accounts, test accounts)
$IgnoreUsers = @('testuser','testuser2')


## Email / SMTP info
# Email credentials stored in seperate file so you can .gitignore
# Example for the json file is in README
$EmailCredsPath = ".\Email_Creds.json"
# For multiple recipients, seperate them by a comma. For example: 'bob@contoso.com,phillip@contoso.com'
$SmtpTo = 'notify@example.com'
# Subject line of the email
$SmtpSubject = "AD Attribute Verification - $(Get-Date -Format g)"


## Gather email creds from Email_Creds.json file
$EmailCreds = Get-Content $EmailCredsPath | ConvertFrom-Json
$SmtpUsername = $EmailCreds[0].SmtpUsername
$SmtpPassword = $EmailCreds[0].SmtpPassword | ConvertTo-SecureString -AsPlainText -Force
# Recipient server
$SmtpServer = $EmailCreds[0].SmtpServer
# Port to send on. 25 = non-tls, 587 = tls
$SmtpPort = $EmailCreds[0].SmtpPort

# Build email credential
$SmtpCredentials = New-Object System.Management.Automation.PSCredential $smtpUsername, $smtpPassword


#############################
# Functions & Classes
#############################

## Add-Log is a function to ensure consistency of logs, as well as allowing future additions to the log without updating the underlying code of the script
## Depends on $CsvLogPath global variable
## Does not return anything
function Add-Log {
    param (
        # Log Type allows for easy filtering. (Ex. Info, AttributeError, Error, FatalError, etc)
        [Parameter(Mandatory=$True)]
        [string]$LogType,

        # Details for the specifics on what is being logged
        [Parameter(Mandatory=$True)]
        [string]$Details,

        # AD ObjectGUID for relevant Active Directory User
        [Parameter(Mandatory=$False)]
        [string]$ADObjectGUID = "",

        # AD attribute not matching approved list
        [Parameter(Mandatory=$False)]
        [string]$Attribute = "",

        # Current value of AD attribute not matching approved list
        [Parameter(Mandatory=$False)]
        [string]$AttributeValue = ""
    )

    # Date Time is in ISO 8601 format
    $DateTime = Get-Date -Format "yyyy-MM-ddTHH:mm:ss"

    # When editing, also edit the "Creating log file" code if needed
    # Columns: Date Time, Log Type, ADObjectGUID, Attribute, AttributeValue, Details
    Add-Content -Path $CsvLogPath -Value ($DateTime + $CsvDelimiter + $LogType + $CsvDelimiter + $ADObjectGUID + $CsvDelimiter + $Attribute + $CsvDelimiter + $AttributeValue + $CsvDelimiter + $Details)

}

## Find-Server is a function to take a distinguished name. Example: OU=Senior Engineers,OU=Engineers,DC=sub,DC=contoso,DC=com
## and convert it into a server address sub.contoso.com
## Returns a server address
function Find-Server() {
    param (
        [Parameter(Mandatory=$True)]
        [String]$DistinguishedName
    )

    # Make final array $ServerAddressArray and $NameArray of individual parts of distinguished name
    $ServerAddressArray = [System.Collections.ArrayList]::new()
    $NameArray = $DistinguishedName.Split(",")

    ## Loop through each part of the distinguished name, and get rid of non "DC=" parts
    foreach ($value in $NameArray){
        # Check to see if it's a DC=*
        if ($value -match '^DC=[a-z,0-9]+$'){
            # Remove the DC= part, add to $ServerName array
            $value = $value -replace '^DC='
            # Re-direct to $null to silence output
            $ServerAddressArray.add($value) > $null
        }
    }

    # String to work on building the server address
    $ServerNameString = ''

    ## Loop through all components of server name, and build into a string
    for ($PartofName=0; $PartofName -lt $ServerAddressArray.count; $PartofName++) {
        # Add a period before if not 
        if ($PartofName -gt 0){
            $ServerNameString = $ServerNameString + '.'
        }
        # Add each part of the address
        $ServerNameString = $ServerNameString + $ServerAddressArray[$PartofName]
    }
    
    return $ServerNameString
}


## Send-EmailReport will send an emailed copy of the results to the recipients $EmailTo
Function Send-EmailReport([String]$EmailBody) {
    $mailParams = @{
        SmtpServer                 = $SmtpServer
        Port                       = $SmtpPort
        UseSSL                     = $true
        Credential                 = $SmtpCredentials
        From                       = $SmtpUsername
        To                         = $SmtpTo
        Subject                    = $SmtpSubject
        Body                       = $EmailBody
        DeliveryNotificationOption = 'OnFailure', 'OnSuccess'
    }
    
    ## Send the message
    Send-MailMessage @mailParams -BodyAsHtml
}

## This is how data on each user with bad attributes will be stored until it is ready to be put in an email
Class BadAttributeUser {
    [string]$ADName
    [string]$AttributeName
    [string]$AttributeValue
    [string]$ADObjectGUID

    BadAttributeUser(
        [string]$ADName,
        [string]$AttributeName,
        [string]$AttributeValue,
        [string]$ADObjectGUID
    ){
        $this.ADName = $ADName
        $this.AttributeName = $AttributeName
        $this.AttributeValue = $AttributeValue
        $this.ADObjectGUID = $ADObjectGUID
    }
}


#############################
# Main
#############################

Clear-Host

## Creating log file
if (Test-Path $CsvLogPath) {
    Write-Host "Log file already exists. Continuing to add data to end of the file"
}
else {
    try { 
        New-Item $CsvLogPath
        Add-Content -Path $CsvLogPath -Value ("Timestamp" + $CsvDelimiter + "Log Type" + $CsvDelimiter + "AD ObjectGUID" + $CsvDelimiter + "AD Attribute" + $CsvDelimiter + "AD Attribute Value" + $CsvDelimiter +  "Details")
    }
    catch {
        # Writing in multiple streams in an attempt to notify an admin since the log file is unavailable
        Write-Host "Error creating and/or writing to log file. Exiting..."
        Write-Error "Error creating and/or writing to log file. Exiting..."
        exit
    }
}

Add-Log -LogType "Info" -Details "Beginning script"

## Verify that the CsvAttributesPath file even exists. If not create it with proper headers
if (-not (Test-Path $CsvAttributesPath)) {
    try {
        New-Item $CsvAttributesPath
        Add-Content -NoNewline -Path $CsvAttributesPath -Value ("Organizational Unit" + $CsvDelimiter + "Attribute")
        # Adding the csv "Attribute Value X" columns
        for ($loop=0; $loop -lt $CsvAttributesNumber; $loop++){
            # -1 is so that the last one has a new line 
            if ($loop -eq ($CsvAttributesNumber - 1)) {
                Add-Content -Path $CsvAttributesPath -Value ($CsvDelimiter + "Attribute Value " + ($loop + 1))
            }
            else { Add-Content -NoNewline -Path $CsvAttributesPath -Value ($CsvDelimiter + "Attribute Value " + ($loop + 1)) }
        }
        

        Write-Host -ForegroundColor Green "Attributes file created. Please fill it out, then re-run this script to begin verification!"
        Add-Log -LogType "Info" -Details "Attributes file empty. Exiting."
        exit
    }
    catch {
        Add-Log -LogType "FatalError" -Details "Error creating Attributes CSV file. Exiting..."
        Write-Error "Error creating Attributes CSV file. Exiting..."
    }
}


## Import the CsvAttributesPath file
$CSVAttributes = Import-Csv -Path $CsvAttributesPath -Delimiter $CsvDelimiter

# Exit if there are no attributes to verify
if ($CSVAttributes.length -eq 0) {
    Add-Log -LogType "FatalError" -Details "No attributes were specified to verify in the Attributes file. Exiting..."
    Write-Error "No attributes were specified to verify in the Attributes file. Exiting..."
    exit
}

# Import the active directory module
try { import-module activedirectory }
catch {
    Add-Log -LogType "FatalError" -Details "Failed to import module activedirectory"
    Write-Error "Failed to import module activedirectory. Exiting."
    exit
}

## Create Arraylist that will be added to when there are people that need to be fixed
# This will be filled with instances of the BadAttributeUser class defined above
$BadAttributeUsers = [System.Collections.ArrayList]::new()

## Loop through each specified OU
foreach ($CSVAttribute in $CSVAttributes){
    # Get all users in the OU
    # Grab the server address
    $ADServer = Find-Server -DistinguishedName $CSVAttribute.'Organizational Unit'

    # Clear variable for subsequent loops through CSVAttributes
    $ADUsers = $null
    $ADUsers = Get-ADUser -Filter * -Server $ADServer -SearchBase $CSVAttribute.'Organizational Unit' -Properties name, EmployeeID, $CSVAttribute.Attribute

    ## Loop through each User adding them to $BadAttributeUsers if their attribute doesn't match
    foreach ($ADUser in $ADUsers) {
        
        ## See if user is in the $IgnoreUsers array, if so skip them
        $SkipUser = $False
        foreach ($IgnoreUser in $IgnoreUsers){
            # Service accounts should have an employeeID of 0
            if ($ADUser.EmployeeID -eq '0'){ 
                $SkipUser = $True
                Write-Host "Skipping $IgnoreUser.name"
                break
            }
            if ($ADUser.SamAccountName -eq $IgnoreUser) { 
                $SkipUser = $True
                Write-Host "Skipping $IgnoreUser.name"
                break
            }
        }

        if ($SkipUser -eq $False) {
            ## Loop through how many attributes are defined by $CsvAttributesNumber
            ## If the field was empty, it will be ignored
            # Assume the user needs to be added to list. Change to true if they match an acceptable attribute
            $AcceptedAttribute = $false
            for ($ValueNumber = 1; $ValueNumber -le $CsvAttributesNumber; $ValueNumber++){

                # Only process if the user defined value is not null
                if (($CSVAttribute."Attribute Value $ValueNumber" -ne '') -or ($null -ne $CSVAttribute."Attribute Value $ValueNumber")){

                    # Check if their value matches a user define attribute
                    if ($CSVAttribute."Attribute Value $ValueNumber" -eq $ADUser."$($CsvAttribute.Attribute)") {
                        $AcceptedAttribute = $True
                        break
                    }
                }
            }

            ## Add user to list of $BadAttributeUsers if $AcceptedAttribute wasn't changed to $True
            if ($AcceptedAttribute -eq $false){
                # Create BadAttributeUser object
                [BadAttributeUser]$BadUser = [BadAttributeUser]::new($ADUser.name, `
                    $CSVAttribute.Attribute, `
                    $ADUser."$($CsvAttribute.Attribute)", `
                    $ADUser.ObjectGUID)
                
                # Sending to $null to prevent output
                $BadAttributeUsers.Add($BadUser) > $null
            }
        }
    }
}

## Format email results of ArrayList if necessary
if ($BadAttributeUsers.Count -ge 1){
    # Create HTML for email
    $EmailBody = $BadAttributeUsers | ConvertTo-Html -Head "Here are the users that do not match the specified attributes. Please correct ASAP. `
        <br><a href=''>KB Guide</a> `
        <br><a href=''>Approved Attributes</a> `
        <br><br>" `
        -Body $_
    
    # Email results
    Send-EmailReport($EmailBody)
}
